import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private messageService: MessageService) {}

  markInvalidFieldsDirty(form: FormGroup) {
    for (const controlName of Object.keys(form.controls)) {
      const control = form.get(controlName);

      if (control instanceof FormControl)
        this.handleFormControl(control);
    }
  }

  private handleFormControl(control: FormControl) {
    if (control.hasError('backendError')) {
      control.updateValueAndValidity();
    }

    if (control.invalid) {
      control?.markAsDirty();
      this.messageService.clear();
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Please verify the provided data'
      });
    }
  }
}
