import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';

import { teamsGuard } from './teams.guard';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MessageService } from 'primeng/api';

describe('teamsGuard', () => {
  let authService: jasmine.SpyObj<AuthService>; 
  
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => teamsGuard(...guardParameters));

  beforeEach(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthService', ['logout']);

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ 
        MessageService,
        { provide: AuthService, useValue: authServiceSpy },
      ]
    });
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should return true if token exists', () => {
    authService.token = 'tokenExample';
    const canActivate = executeGuard({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
    expect(canActivate).toBeTrue();
  });

  it('should return false and call authService.logout if token does not exist', () => {
    authService.token = null;
    const canActivate = executeGuard({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
    expect(canActivate).toBeFalse();
    expect(authService.logout).toHaveBeenCalled();
  });
});
