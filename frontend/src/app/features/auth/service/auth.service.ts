import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment.development';
import { catchError } from 'rxjs';
import { ErrorHandlerService } from '../../../services/error-handler.service';

interface LoginResponse {
  token: string;
  role: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string | null = null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private cookieSrvc: CookieService,
    private errorHandlerService: ErrorHandlerService) {
      const token = this.cookieSrvc.get('jwt');
      
      if (token)
        this.token = token;
  }

  login(credentials: any) {
    this.http.post<LoginResponse>(environment.apiBase + 'auth/login', credentials)
      .pipe(catchError(err => this.errorHandlerService.handleError(err)))
      .subscribe(res => {
        this.cookieSrvc.set('jwt', res.token, 1, undefined, undefined, true);
        this.token = res.token;
        this.router.navigateByUrl('/teams');
      });
  }

  logout() {
    this.token = null;
    this.cookieSrvc.delete('jwt');
    this.router.navigateByUrl('auth/login');
  }
}
