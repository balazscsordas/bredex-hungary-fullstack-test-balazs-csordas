import { Component, OnInit } from '@angular/core';
import { TeamsService } from './service/teams.service';
import { Team } from '../../models/Team';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrl: './teams.component.scss'
})
export class TeamsComponent implements OnInit {
  teams: Team[] = [];
  loading = true;

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private teamsService: TeamsService,
    private confirmService: ConfirmationService,
  ) {}

  ngOnInit(): void {
    this.teamsService.getTeams();

    this.teamsService.teams.pipe(takeUntil(this.destroy$)).subscribe(teams => {
      this.teams = teams;
      this.loading = false;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  navToAddTeamPage() {
    this.router.navigateByUrl('teams/add-team');
  }
 
  navToEditTeam(name: string, id: number) {
    this.router.navigateByUrl(`teams/${name}/${id}`)
  }

  openDelTeamDialog(id: number) {
    this.confirmService.confirm({
      icon: 'pi pi-exclamation-triangle',
      header: 'Delete Team',
      message: 'Are you sure you want to delete the Team?',
      rejectLabel: 'No',
      acceptLabel: 'Yes',
      accept: () => this.teamsService.deleteTeam(id),
    });
  }
}
