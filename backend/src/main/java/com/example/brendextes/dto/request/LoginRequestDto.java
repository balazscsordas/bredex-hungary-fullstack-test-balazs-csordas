package com.example.brendextes.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class LoginRequestDto {
    @NotBlank(message = "Please provide the missing Username")
    @Size(max = 150, message = "Maximum length for Username is 150 characters")
    private String username;

    @NotBlank(message = "Please provide the missing Password")
    @Size(max = 150, message = "Maximum length for Password is 150 characters")
    private String password;
}