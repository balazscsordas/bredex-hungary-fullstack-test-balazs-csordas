package com.example.brendextes.service.Impl;

import com.example.brendextes.dto.request.AddTeamRequestDto;
import com.example.brendextes.dto.request.EditTeamRequestDto;
import com.example.brendextes.dto.response.EmptyResponse;
import com.example.brendextes.entity.Team;
import com.example.brendextes.exception.ConflictException;
import com.example.brendextes.exception.ResourceNotFoundException;
import com.example.brendextes.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TeamService {

    private final TeamRepository teamRepo;

    public EmptyResponse addTeam(AddTeamRequestDto teamDetails) {
        String name = teamDetails.getName().trim();
        Optional<Team> teamNameCheck = this.teamRepo.findByName(name);
        if (teamNameCheck.isPresent())
            throw new ConflictException(String.format("There is already a Team with name: %s", name), "name");

        Team team = new Team(
                name,
                teamDetails.getFoundingYear(),
                teamDetails.getNumberOfTitles(),
                teamDetails.isPaidRegistrationFee()
        );

        this.teamRepo.save(team);
        return new EmptyResponse(String.format("Successfully added a new Team with ID: %s", team.getId()));
    }

    public Team getTeam(Long id) {
        return this.teamRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("There isn't any Team with ID: %s", id)));
    }

    public List<Team> getTeams() {
        return this.teamRepo.findAll();
    }

    public EmptyResponse editTeam(EditTeamRequestDto teamDetails) {
        Team team = this.teamRepo.findById(teamDetails.getId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("There isn't any Team with ID: %s", teamDetails.getId())));

        team.setName(teamDetails.getName().trim());
        team.setFoundingYear(teamDetails.getFoundingYear());
        team.setNumberOfTitles(team.getNumberOfTitles());
        team.setPaidRegistrationFee(team.isPaidRegistrationFee());

        this.teamRepo.save(team);
        return new EmptyResponse(String.format("Successfully updated the Team with ID: %s", team.getId()));
    }

    public EmptyResponse deleteTeam(Long id) {
        Team team = this.teamRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("There isn't any Team with ID: %s", id)));

        this.teamRepo.delete(team);
        return new EmptyResponse(String.format("Successfully deleted the Team with ID: %s", id));
    }
}
