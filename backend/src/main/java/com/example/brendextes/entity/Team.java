package com.example.brendextes.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    private Integer foundingYear;

    private Integer numberOfTitles;

    private boolean paidRegistrationFee;

    public Team(String name, Integer foundingYear, Integer numberOfTitles, boolean paidRegistrationFee) {
        this.name = name;
        this.foundingYear = foundingYear;
        this.numberOfTitles = numberOfTitles;
        this.paidRegistrationFee = paidRegistrationFee;
    }
}
