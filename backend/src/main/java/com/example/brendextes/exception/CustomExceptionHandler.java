package com.example.brendextes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(ResourceNotFoundException ex) {
        return new ErrorMessage(
                HttpStatus.NOT_FOUND,
                ex.getMessage(),
                "Resource Not Found");
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorMessage conflictException(ConflictException ex) {
        return new ErrorMessage(
                HttpStatus.CONFLICT,
                ex.getMessage(),
                "Conflict",
                ex.getInputField());
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public ErrorMessage unauthorizedException(UnauthorizedException ex) {
        return new ErrorMessage(
                HttpStatus.UNAUTHORIZED,
                ex.getMessage(),
                "Unauthorized");
    }
}
