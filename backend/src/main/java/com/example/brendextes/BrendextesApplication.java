package com.example.brendextes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrendextesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrendextesApplication.class, args);
	}

}
