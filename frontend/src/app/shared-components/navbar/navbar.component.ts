import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../features/auth/service/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  showMobileSidebar = false;

  constructor(
    private router: Router,
    public  authService: AuthService) {}

  navToLoginPage() {
    this.router.navigateByUrl('auth/login');
  }

  closeMobileSidebar() {
    this.showMobileSidebar = false;
  }

  toggleMobileSidebar() {
    this.showMobileSidebar = !this.showMobileSidebar;
  }

  logout() {
    this.authService.logout();
  }
}
