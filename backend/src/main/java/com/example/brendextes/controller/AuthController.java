package com.example.brendextes.controller;

import com.example.brendextes.dto.request.LoginRequestDto;
import com.example.brendextes.dto.response.LoginResponseDto;
import com.example.brendextes.service.Impl.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
public class AuthController {
    private final AuthService authService;
    @PostMapping("login")
    public ResponseEntity<LoginResponseDto> login(@Valid @RequestBody LoginRequestDto loginCredentials) {
        return ResponseEntity.ok(authService.login(loginCredentials));
    }
}
