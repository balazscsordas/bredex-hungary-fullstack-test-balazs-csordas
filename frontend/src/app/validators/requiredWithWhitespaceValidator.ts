import { AbstractControl, ValidationErrors } from '@angular/forms';

export function requiredWithWhitespace(control: AbstractControl): ValidationErrors | null {
  const value = control.getRawValue();

  if (value == null || value?.trim().length == 0) {
    return { 'required': true, 'whitespace': true };
  }

  return null;
}