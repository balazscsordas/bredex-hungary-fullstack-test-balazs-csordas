package com.example.brendextes.service.Impl;

import com.example.brendextes.dto.request.LoginRequestDto;
import com.example.brendextes.dto.response.LoginResponseDto;
import com.example.brendextes.entity.User;
import com.example.brendextes.exception.ConflictException;
import com.example.brendextes.repository.UserRepository;
import com.example.brendextes.service.JwtTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepo;
    private final JwtTokenService jwtService;

    public LoginResponseDto login(LoginRequestDto loginCredentials) {
        User user = this.userRepo.findByUsername(loginCredentials.getUsername())
                .orElseThrow(() -> new ConflictException("Invalid credentials, please try again"));

        if (user.getPassword().equals(loginCredentials.getPassword())) {
            Map<String, Object> claims = Map.of("role", user.getRole(), "userId", user.getId());
            return new LoginResponseDto(jwtService.createToken(claims, user.getUsername()));
        } else
            throw new ConflictException("Invalid credentials, please try again");
    }
}
