import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { requiredWithWhitespace } from '../../../../validators/requiredWithWhitespaceValidator';
import { FormService } from '../../../../services/form.service';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formService: FormService) {
      this.loginForm = new FormGroup({
        username: new FormControl<string | null>(null, [requiredWithWhitespace]),
        password: new FormControl<string | null>(null, [requiredWithWhitespace]),
      }, { updateOn: 'submit' });
  }

  ngOnInit(): void {
    this.authService.logout();
  }

  login() {
    this.formService.markInvalidFieldsDirty(this.loginForm);
    if (this.loginForm.valid)
      this.authService.login(this.loginForm.value);
  }

  navToRegistrationPage() {
    this.router.navigateByUrl('auth/registration');
  }
}
