package com.example.brendextes.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class EditTeamRequestDto extends AddTeamRequestDto {
    @NotNull(message = "Please provide the missing ID")
    private Long id;
}
