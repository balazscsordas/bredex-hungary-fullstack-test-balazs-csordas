import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamsComponent } from './teams.component';

const routes: Routes = [
  { path: '', component: TeamsComponent },
  { path: 'add-team',  loadChildren: () => import('./features/team-details/team-details.module').then(m => m.TeamDetailsModule)},
  { path: ':name/:id', loadChildren: () => import('./features/team-details/team-details.module').then(m => m.TeamDetailsModule)},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule { }
