package com.example.brendextes.dto.request;

import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class AddTeamRequestDto {
    @NotBlank(message = "Please provide the missing Name")
    @Size(max = 150, message = "Maximum length for Name is 150 characters")
    private String name;

    //TODO: add max validation
    @NotNull(message = "Please provide the missing Founding Year")
    @Min(value = 1950, message = "Minimum value for Founding Year is 1950")
    private Integer foundingYear;

    @NotNull(message = "Please provide the missing Founding Year")
    @Min(value = 0, message = "Minimum value for Number Of Titles is 0")
    private Integer numberOfTitles;

    @NotNull(message = "Please provide the missing Paid Registration Fee")
    private boolean paidRegistrationFee;
}
