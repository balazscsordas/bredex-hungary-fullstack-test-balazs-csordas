import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private router: Router,
    private cookieService: CookieService,
    private messageService: MessageService) {}

  handleError(err: HttpErrorResponse, form?: FormGroup) {
    if (err.status == 0) {
      this.messageService.clear()
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Server is not responding!' });
    } 
    else if (err.status == 401) {
      this.cookieService.delete('jwt');
      this.router.navigateByUrl('auth/login');
    }
    else if (err.status == 500) {
      this.messageService.clear()
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something bad happened, please try again later' });
    } 
    else {
      (form && err.error.inputField) &&
        form.get(err.error.inputField)?.setErrors({ backendError: true });

      this.messageService.clear()
      this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Please verify the provided data' });
    }

    return throwError(() => new Error('Something bad happened, please try again later.'));
  }
}
