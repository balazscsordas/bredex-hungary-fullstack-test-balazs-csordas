import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { requiredWithWhitespace } from '../../../../validators/requiredWithWhitespaceValidator';
import { FormService } from '../../../../services/form.service';
import { ActivatedRoute } from '@angular/router';
import { TeamsService } from '../../service/teams.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrl: './team-details.component.scss'
})
export class TeamDetailsComponent implements OnInit {
  teamForm: FormGroup;
  id?: number;
  name?: string;
  maxYear: number;

  private destroy$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private formService: FormService,
    private teamService: TeamsService) {
      this.id = this.route.snapshot.params['id'];
      this.name = this.route.snapshot.params['name'];
      
      const today = new Date();
      this.maxYear = today.getFullYear();

      this.teamForm = new FormGroup({
        id: new FormControl<number | null>(null),
        name: new FormControl<string | null>(null, [requiredWithWhitespace]),
        foundingYear: new FormControl<number | null>(null, [Validators.required, Validators.min(1950), Validators.max(this.maxYear)]),
        numberOfTitles: new FormControl<number | null>(null, [Validators.required, Validators.min(0)]),
        paidRegistrationFee: new FormControl<boolean>(false),
      }, { updateOn: 'submit' });
  }

  ngOnInit(): void {
    if (this.id) {
      this.teamService.getTeam(this.id);

      this.teamService.team.pipe(takeUntil(this.destroy$)).subscribe(team => {
        this.teamForm.patchValue(team);
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  submit() {
    this.formService.markInvalidFieldsDirty(this.teamForm);
    if (this.teamForm.valid) {
      this.id
        ? this.teamService.editTeam(this.teamForm.value, this.teamForm)
        : this.teamService.addTeam(this.teamForm.value, this.teamForm);
    }
  }
}
