package com.example.brendextes.controller;

import com.example.brendextes.dto.request.AddTeamRequestDto;
import com.example.brendextes.dto.request.EditTeamRequestDto;
import com.example.brendextes.dto.response.EmptyResponse;
import com.example.brendextes.entity.Team;
import com.example.brendextes.service.Impl.TeamService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/team")
public class TeamController {
    private final TeamService teamService;

    @PostMapping()
    public ResponseEntity<EmptyResponse> addTeam(@Valid @RequestBody AddTeamRequestDto teamDetails) {
        return ResponseEntity.ok(teamService.addTeam(teamDetails));
    }
    @GetMapping("/{id}")
    public ResponseEntity<Team> getTeam(@Valid @NotNull(message = "Please provide a valid ID") @PathVariable Long id) {
        return ResponseEntity.ok(teamService.getTeam(id));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Team>> getTeams() {
        return ResponseEntity.ok(teamService.getTeams());
    }

    @PutMapping()
    public ResponseEntity<EmptyResponse> editTeam(@Valid @RequestBody EditTeamRequestDto teamDetails) {
        return ResponseEntity.ok(teamService.editTeam(teamDetails));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<EmptyResponse> deleteTeam(@Valid @NotNull(message = "Please provide a valid ID") @PathVariable Long id) {
        return ResponseEntity.ok(teamService.deleteTeam(id));
    }
}
