CREATE TABLE teams (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  founding_year INTEGER,
  number_of_titles INTEGER,
  paid_registration_fee BOOLEAN NOT NULL
);

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  role VARCHAR(50) NOT NULL
);
INSERT INTO users (username, password, role) VALUES ('admin', 'f1test2018', 'ADMIN');
