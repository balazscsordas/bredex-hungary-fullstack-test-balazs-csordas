import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { AuthService } from '../../auth/service/auth.service';

export const teamsGuard: CanActivateFn = (route, state) => {
  const authService = inject(AuthService);

  if (authService.token)
    return true;
  else {
    authService.logout();
    return false;
  }
};
