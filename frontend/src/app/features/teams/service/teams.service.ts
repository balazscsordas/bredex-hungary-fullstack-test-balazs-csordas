import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, OnDestroy, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ErrorHandlerService } from '../../../services/error-handler.service';
import { environment } from '../../../../environments/environment.development';
import { Subject, catchError, takeUntil } from 'rxjs';
import { Team } from '../../../models/Team';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TeamsService implements OnDestroy {
  @Output() team = new EventEmitter<Team>;
  @Output() teams = new EventEmitter<Team[]>;

  private destroy$ = new Subject<void>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private messageService: MessageService,
    private errorHandlerService: ErrorHandlerService) {}

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  addTeam(teamDetails: any, form: FormGroup) {
    this.http.post<any>(environment.apiBase + 'team', teamDetails)
      .pipe(
        catchError(err => this.errorHandlerService.handleError(err, form)),
        takeUntil(this.destroy$))
      .subscribe(res => {
        this.router.navigateByUrl('teams');
        this.messageService.clear();
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Successfully added a new Team' });
      })
  }

  getTeams() {
    this.http.get<Team[]>(environment.apiBase + 'team/all')
      .pipe(
        catchError(err => this.errorHandlerService.handleError(err)),
        takeUntil(this.destroy$))
      .subscribe(teams => {
        this.teams.emit(teams);
      });
  }

  getTeam(id: number) {
    this.http.get<Team>(environment.apiBase + `team/${id}`)
      .pipe(
        catchError(err => this.errorHandlerService.handleError(err)),
        takeUntil(this.destroy$))
      .subscribe(team => {
        this.team.emit(team);
      });
  }

  editTeam(teamDetails: any, form: FormGroup) {
    this.http.put<any>(environment.apiBase + 'team', teamDetails)
      .pipe(
        catchError(err => this.errorHandlerService.handleError(err, form)),
        takeUntil(this.destroy$))
      .subscribe(res => {
        this.router.navigateByUrl('teams');
        this.messageService.clear();
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Successfully edited the Team' });
      })
  }

  deleteTeam(teamId: number) {
    this.http.delete<any>(environment.apiBase + `team/${teamId}`)
      .pipe(
        catchError(err => this.errorHandlerService.handleError(err)),
        takeUntil(this.destroy$))
      .subscribe(res => {
        this.getTeams();
        this.messageService.clear();
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Successfully deleted the Team' });
      })
  }
}
