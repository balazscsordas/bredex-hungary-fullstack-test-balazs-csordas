export interface Team {
    id: number;
    name: string;
    foundingYear: number;
    numberOfTitles: number;
    paidRegistrationFee: boolean
}