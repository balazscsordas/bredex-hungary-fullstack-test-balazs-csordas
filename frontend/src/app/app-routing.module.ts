import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from './shared-components/not-found-page/not-found-page.component';
import { teamsGuard } from './features/teams/guard/teams.guard';

const routes: Routes = [
  { path: '',      redirectTo: 'auth/login', pathMatch: 'full' },
  { path: 'auth',  loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule) },
  { path: 'teams', loadChildren: () => import('./features/teams/teams.module').then(m => m.TeamsModule), canActivate: [teamsGuard] },
  { path: '**', component: NotFoundPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
