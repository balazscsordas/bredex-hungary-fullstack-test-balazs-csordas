import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsRoutingModule } from './teams-routing.module';
import { TeamsComponent } from './teams.component';

import { ButtonModule } from 'primeng/button';
import { SkeletonModule } from 'primeng/skeleton';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [
    TeamsComponent,
  ],
  imports: [
    CommonModule,
    TeamsRoutingModule,

    SkeletonModule,
    ButtonModule,
    TooltipModule,
  ]
})
export class TeamsModule { }