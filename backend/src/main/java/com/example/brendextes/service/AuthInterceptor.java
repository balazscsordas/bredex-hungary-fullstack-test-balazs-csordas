package com.example.brendextes.service;

import com.example.brendextes.exception.UnauthorizedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
@RequiredArgsConstructor
public class AuthInterceptor implements HandlerInterceptor {
    private final JwtTokenService jwtService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String auth = request.getHeader("Authorization");

        if (auth != null && !auth.isEmpty()) {
            String[] authSplit = auth.split(" ");

            if (authSplit.length == 2 || authSplit[0].equals("Bearer")) {
                String token = authSplit[1];

                try {
                    if (!jwtService.isTokenExpired(token)) {
                        Claims claims = this.jwtService.extractAllClaims(token);
                        request.setAttribute("claims", claims);
                        return true;

                    } else throw new UnauthorizedException("Token is expired");
                } catch (JwtException e) {
                    throw new UnauthorizedException("Invalid token");
                }
            } else throw new UnauthorizedException("Invalid token format");
        } else throw new UnauthorizedException("Unauthorized");
    }
}
