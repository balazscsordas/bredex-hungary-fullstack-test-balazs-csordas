import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TeamDetailsRoutingModule } from './team-details-routing.module';
import { TeamDetailsComponent } from './team-details.component';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';

@NgModule({
  declarations: [
    TeamDetailsComponent
  ],
  imports: [
    CommonModule,
    TeamDetailsRoutingModule,
    ReactiveFormsModule,
    FormsModule,

    ButtonModule,
    InputTextModule,
    CheckboxModule,
  ]
})
export class TeamDetailsModule { }
