Steps:

1. Download Docker and go through the installation process
2. Clone the repository
3. Open a terminal and navigate to the folder where the "docker-compose.yml" file exists
4. Run the following command: "docker-compose up" and wait until Docker sets up the environment
5. Open a browser and visit "http://localhost:4200"
