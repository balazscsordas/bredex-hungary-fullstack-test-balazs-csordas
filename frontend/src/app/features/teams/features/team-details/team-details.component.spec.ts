import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TeamDetailsComponent } from './team-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('TeamDetailsComponent', () => {
  let component: TeamDetailsComponent;
  let fixture: ComponentFixture<TeamDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, CheckboxModule, ReactiveFormsModule, FormsModule ],
      declarations: [ TeamDetailsComponent ],
      providers: [
        MessageService,
        { provide: ActivatedRoute, useValue: { snapshot: { params: { ['id']: '1', ['name']: 'TeamName' } } } },
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TeamDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form with correct controls and validators', () => {
    const controls = component.teamForm.controls;
    expect(controls['id']).toBeDefined();
    expect(controls['name']).toBeDefined();
    expect(controls['foundingYear']).toBeDefined();
    expect(controls['numberOfTitles']).toBeDefined();
    expect(controls['numberOfTitles']).toBeDefined();
    expect(controls['paidRegistrationFee']).toBeDefined();

    expect(controls['name'].validator).toBeDefined();
    expect(controls['foundingYear'].validator).toBeDefined();
    expect(controls['numberOfTitles'].validator).toBeDefined();
  });
});
