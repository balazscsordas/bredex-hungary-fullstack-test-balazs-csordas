import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing'; 
import { ConfirmationService, MessageService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { NavbarComponent } from './shared-components/navbar/navbar.component';
import { SidebarModule } from 'primeng/sidebar';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule, ToastModule, ConfirmDialogModule, SidebarModule ],
      declarations: [ AppComponent, NavbarComponent ],
      providers: [ MessageService, ConfirmationService ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'frontend'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('F1 app');
  });

  it('should render the app-navbar component', () => {
    const navbarElement = fixture.debugElement.queryAll(By.css('app-navbar'));
    expect(navbarElement[0].nativeElement).toBeTruthy();
  });

  it('should render the p-confirmDialog component', () => {
    const navbarElement = fixture.debugElement.queryAll(By.css('p-confirmDialog'));
    expect(navbarElement[0].nativeElement).toBeTruthy();
  });

  it('should render the p-toast component', () => {
    const navbarElement = fixture.debugElement.queryAll(By.css('p-toast'));
    expect(navbarElement[0].nativeElement).toBeTruthy();
  });
});
