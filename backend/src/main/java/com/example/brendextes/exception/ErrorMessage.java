package com.example.brendextes.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

import java.util.Date;

@Getter
@Setter
public class ErrorMessage {
    private HttpStatusCode statusCode;
    private Date timestamp;
    private String message;
    private String description;
    private String inputField;

    public ErrorMessage(HttpStatus statusCode, String message, String description) {
        this.statusCode = statusCode;
        this.timestamp = new Date();
        this.message = message;
        this.description = description;
    }

    public ErrorMessage(HttpStatusCode statusCode, String message, String description, String inputField) {
        this.statusCode = statusCode;
        this.timestamp = new Date();
        this.message = message;
        this.description = description;
        this.inputField = inputField;
    }
}
