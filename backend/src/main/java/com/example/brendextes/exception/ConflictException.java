package com.example.brendextes.exception;

import lombok.Getter;

@Getter
public class ConflictException extends RuntimeException {
    private String inputField;

    public ConflictException(String message, String inputField) {
        super(message);
        this.inputField = inputField;
    }

    public ConflictException(String message) {
        super(message);
        this.inputField = null;
    }
}
